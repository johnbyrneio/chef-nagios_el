name              'nagios_el'
maintainer        'John Byrne'
maintainer_email  'johnpbyrne@gmail.com'
license           'Apache 2.0'
description       'Installs and configures Nagios server and the NRPE client on RHEL and CentOS'
long_description  IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version           '1.0.0'

recipe 'nagios', 'Includes the client recipe.'
recipe 'nagios::client', 'Installs and configures a nrpe client'
recipe 'nagios::server', 'Installs and configures a nagios server'
recipe 'nagios::pagerduty', 'Integrates contacts w/ PagerDuty API'

depends 'yum-epel'

supports 'redhat'
supports 'centos'
