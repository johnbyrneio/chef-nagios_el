include_recipe "yum-epel"

package "nagios" do
	action :install
end

package "nagios-plugins-nrpe" do
	action :install
end

service "httpd" do
	action [ :enable, :start ]
end

service "nagios" do
	action [ :enable, :start ]
end
