include_recipe 'yum-epel'

# install the nrpe packages specified in the ['nrpe']['packages'] attribute
node['nagios']['nrpe']['packages'].each do |pkg|
  package pkg
end
